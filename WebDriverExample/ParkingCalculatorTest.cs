﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace WebDriverExample
{
    [TestClass]
    public class ParkingCalculatorTest
    {
        [TestMethod]
        public void CalculatorShouldReturnCorrectCost()
        {
            var driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("http://adam.goucher.ca/parkcalc/");

            var entryDateInput = driver.FindElement(By.Id("EntryDate"));
            entryDateInput.Clear();
            entryDateInput.SendKeys("2/16/2016");

            var leavingDateInput = driver.FindElement(By.Id("ExitDate"));
            leavingDateInput.Clear();
            leavingDateInput.SendKeys("2/17/2016");


            var calculateButton = driver.FindElement(By.XPath("/html/body/form/input[2]"));
            calculateButton.Click();

            var cost = driver.FindElement(By.XPath("/html/body/form/table/tbody/tr[4]/td[2]/span[1]/font/b"));

            Assert.AreEqual("$ 28.00", cost.Text);

            driver.Close();
            
        }
    }
}
