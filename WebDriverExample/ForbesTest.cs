﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace WebDriverExample
{
    [TestClass]
    public class ForbesTest
    {
        [TestMethod]
        public void ShouldDisplayWelcomeMessage()
        {
            var driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("http://www.forbes.com");
            Assert.AreEqual("http://www.forbes.com/forbes/welcome/", driver.Url);
            driver.Close();
        }
    }
}
