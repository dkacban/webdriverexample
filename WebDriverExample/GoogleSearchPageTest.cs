﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace WebDriverExample
{
    [TestClass]
    public class GoogleSearchPageTest
    {
        [TestMethod]
        public void ShouldPresentCorrectNumberOfSearchResults()
        {
            var driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("http://www.google.pl");
            var input = driver.FindElement(By.Id("lst-ib"));
            input.SendKeys("webdriver");
            input.Submit();

            Thread.Sleep(1000);

            var result = driver.FindElementsByTagName("h3");
            Assert.AreEqual(11, result.Count);

            driver.Close();
        }

        [TestMethod]
        public void ShouldDisplaySearchInGoogleButton()
        {
            var driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("http://www.google.pl");
            Thread.Sleep(1000);
            var searchButton = driver.FindElement(By.Name("btnK"));
            Assert.IsNotNull(searchButton);
            Assert.AreEqual("Szukaj w Google", searchButton.GetAttribute("value"));
            driver.Close();
        }

        [TestMethod]
        public void ShouldDisplayRandomSearchButton()
        {
            var driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("http://www.google.pl");
            Thread.Sleep(1000);
            var searchButton = driver.FindElement(By.Name("btnI"));
            Assert.IsNotNull(searchButton);
            Assert.AreEqual("Szczęśliwy traf", searchButton.GetAttribute("value"));
            driver.Close();
        }
    }
}
